﻿const phoneBlock = document.getElementById("phone");
const pairPhoneBlock = document.getElementById("pairPhone");
const inputBox = document.getElementById("input-box");
const ansBtn = document.getElementById("answer-call");
const endBtn = document.getElementById("end-call");
const contactNameInfo = document.getElementById("contactName");
const contactPhoneInfo = document.getElementById("contactPhone");
const dialInputBox = document.getElementById("dial-input");
const callStatus = document.getElementById("call-status");
const pairNumberList = document.getElementById("paired-number-list");
const pairedbtn = document.getElementById("paired-btn");
const pairHint = document.getElementById("pairHint");
const countryInput = document.getElementById("countryInput");
const nameInput = document.getElementById("nameInput");
const phoneInput = document.getElementById("phoneInput");
const extensionInput = document.getElementById("extensionInput");
const verifyPhoneBtn = document.getElementById("verifyPhoneButton");
const callingPhone = document.getElementById("inCallingPhone");
const verificationCode = document.getElementById("verificationCode");
const beforePairBlock = document.getElementById("beforePairBlock");
const inParingBlock = document.getElementById("inParingBlock");
const backBtn = document.getElementById("backBtn");
const displayPair = document.getElementById("displayPaired");
const COUNTRYARRAY = [
  "Afghanistan  (+93)",
  "Albania  (+355)",
  "Algeria  (+213)",
  "American Samoa  (+1684)",
  "Andorra  (+376)",
  "Angola  (+244)",
  "Anguilla  (+1264)",
  "Antigua and Barbuda  (+1268)",
  "Argentina  (+54)",
  "Armenia  (+374)",
  "Aruba  (+297)",
  "Ascension  (+247)",
  "Australia/Cocos/Christmas Island  (+61)",
  "Austria  (+43)",
  "Azerbaijan  (+994)",
  "Bahamas  (+1242)",
  "Bahrain  (+973)",
  "Bangladesh  (+880)",
  "Barbados  (+1246)",
  "Belarus  (+375)",
  "Belgium  (+32)",
  "Belize  (+501)",
  "Benin  (+229)",
  "Bermuda  (+1441)",
  "Bhutan  (+975)",
  "Bolivia  (+591)",
  "Bosnia and Herzegovina  (+387)",
  "Botswana  (+267)",
  "Brazil  (+55)",
  "Brunei  (+673)",
  "Bulgaria  (+359)",
  "Burkina Faso  (+226)",
  "Burundi  (+257)",
  "Cambodia  (+855)",
  "Cameroon  (+237)",
  "Canada  (+1)",
  "Canary Islands  (+3491)",
  "Cape Verde  (+238)",
  "Cayman Islands  (+1345)",
  "Central Africa  (+236)",
  "Chad  (+235)",
  "Chile  (+56)",
  "China  (+86)",
  "Colombia  (+57)",
  "Comoros  (+269)",
  "Congo  (+242)",
  "Congo, Dem Rep  (+243)",
  "Cook Islands  (+682)",
  "Costa Rica  (+506)",
  "Croatia  (+385)",
  "Cuba  (+53)",
  "Cyprus  (+357)",
  "Czech Republic  (+420)",
  "Denmark  (+45)",
  "Djibouti  (+253)",
  "Dominica  (+1767)",
  "Dominican Republic  (+1809)",
  "Dominican Republic  (+1809201)",
  "Dominican Republic  (+1829)",
  "Dominican Republic  (+1849)",
  "East Timor  (+670)",
  "Ecuador  (+593)",
  "Egypt  (+20)",
  "El Salvador  (+503)",
  "Equatorial Guinea  (+240)",
  "Eritrea  (+291)",
  "Estonia  (+372)",
  "Ethiopia  (+251)",
  "Falkland Islands  (+500)",
  "Faroe Islands  (+298)",
  "Fiji  (+679)",
  "Finland/Aland Islands  (+358)",
  "France  (+33)",
  "French Guiana  (+594)",
  "French Polynesia  (+689)",
  "Gabon  (+241)",
  "Gambia  (+220)",
  "Georgia  (+995)",
  "Germany  (+49)",
  "Ghana  (+233)",
  "Gibraltar  (+350)",
  "Greece  (+30)",
  "Greenland  (+299)",
  "Grenada  (+1473)",
  "Guadeloupe  (+590)",
  "Guam  (+1671)",
  "Guam  (+1671)",
  "Guatemala  (+502)",
  "Guinea  (+224)",
  "Guinea-Bissau  (+245)",
  "Guyana  (+592)",
  "Haiti  (+509)",
  "Honduras  (+504)",
  "Hong Kong  (+852)",
  "Hungary  (+36)",
  "Iceland  (+354)",
  "India  (+91)",
  "Indonesia  (+62)",
  "Iran  (+98)",
  "Iraq  (+964)",
  "Ireland  (+353)",
  "Israel  (+972)",
  "Italy  (+39)",
  "Ivory Coast  (+225)",
  "Jamaica  (+1876)",
  "Japan  (+81)",
  "Jordan  (+962)",
  "Kenya  (+254)",
  "Kiribati  (+686)",
  "Korea Dem People's Rep  (+850)",
  "Korea Republic of  (+82)",
  "Kosovo  (+383)",
  "Kuwait  (+965)",
  "Kyrgyzstan  (+996)",
  "Laos PDR  (+856)",
  "Latvia  (+371)",
  "Lebanon  (+961)",
  "Lesotho  (+266)",
  "Liberia  (+231)",
  "Libya  (+218)",
  "Liechtenstein  (+423)",
  "Lithuania  (+370)",
  "Luxembourg  (+352)",
  "Macau  (+853)",
  "Macedonia  (+389)",
  "Madagascar  (+261)",
  "Malawi  (+265)",
  "Malaysia  (+60)",
  "Maldives  (+960)",
  "Mali  (+223)",
  "Malta  (+356)",
  "Marshall Islands  (+692)",
  "Martinique  (+596)",
  "Mauritania  (+222)",
  "Mauritius  (+230)",
  "Mexico  (+52)",
  "Micronesia  (+691)",
  "Moldova  (+373)",
  "Monaco  (+377)",
  "Mongolia  (+976)",
  "Montenegro  (+382)",
  "Montserrat  (+1664)",
  "Morocco/Western Sahara  (+212)",
  "Mozambique  (+258)",
  "Myanmar  (+95)",
  "Namibia  (+264)",
  "Nepal  (+977)",
  "Netherlands  (+31)",
  "Netherlands Antilles  (+599)",
  "New Caledonia  (+687)",
  "New Zealand  (+64)",
  "Nicaragua  (+505)",
  "Niger  (+227)",
  "Nigeria  (+234)",
  "Niue  (+683)",
  "Norfolk Island  (+672)",
  "Northern Mariana Islands  (+1670)",
  "Norway  (+47)",
  "Oman  (+968)",
  "Pakistan  (+92)",
  "Palau  (+680)",
  "Palestinian Territory  (+970)",
  "Panama  (+507)",
  "Papua New Guinea  (+675)",
  "Paraguay  (+595)",
  "Peru  (+51)",
  "Philippines  (+63)",
  "Poland  (+48)",
  "Portugal  (+351)",
  "Puerto Rico  (+1787)",
  "Qatar  (+974)",
  "Reunion/Mayotte  (+262)",
  "Romania  (+40)",
  "Russia/Kazakhstan  (+7)",
  "Rwanda  (+250)",
  "Samoa  (+685)",
  "San Marino  (+378)",
  "Sao Tome and Principe  (+239)",
  "Saudi Arabia  (+966)",
  "Senegal  (+221)",
  "Serbia  (+381)",
  "Seychelles  (+248)",
  "Sierra Leone  (+232)",
  "Singapore  (+65)",
  "Slovakia  (+421)",
  "Slovenia  (+386)",
  "Solomon Islands  (+677)",
  "Somalia  (+252)",
  "South Africa  (+27)",
  "South Sudan  (+211)",
  "Spain  (+34)",
  "Spain  (+34)",
  "Sri Lanka  (+94)",
  "St Kitts and Nevis  (+1869)",
  "St Lucia  (+1758)",
  "St Pierre and Miquelon  (+508)",
  "St Vincent Grenadines  (+1784)",
  "Sudan  (+249)",
  "Suriname  (+597)",
  "Swaziland  (+268)",
  "Sweden  (+46)",
  "Switzerland  (+41)",
  "Syria  (+963)",
  "Taiwan  (+886)",
  "Tajikistan  (+992)",
  "Tanzania  (+255)",
  "Thailand  (+66)",
  "Togo  (+228)",
  "Tonga  (+676)",
  "Trinidad and Tobago  (+1868)",
  "Tunisia  (+216)",
  "Turkey  (+90)",
  "Turkish Republic of Northern Cyprus  (+90)",
  "Turkmenistan  (+993)",
  "Turks and Caicos Islands  (+1649)",
  "Tuvalu  (+688)",
  "Uganda  (+256)",
  "Ukraine  (+380)",
  "United Arab Emirates  (+971)",
  "United Kingdom  (+44)",
  "Uruguay  (+598)",
  "Uzbekistan  (+998)",
  "Vanuatu  (+678)",
  "Vatican City  (+379)",
  "Venezuela  (+58)}",
  "Vietnam  (+84)",
  "Virgin Islands, British  (+1284)",
  "Virgin Islands, U.S.  (+1340)",
  "Yemen  (+967)",
  "Zambia  (+260)",
  "Zimbabwe  (+263)",
];
// Passed from Intergration or the main page
let ACCOUNT_SID = "AC85bf9bcaffa1f4c225e13498164b60dc";
let AUTH_TOKEN = "48d0f8dfe66964cb3d37ee2c724dc7d3";
let FAWEBHOOKURL =
  "https://dev.freeagent.network/webhook/4963d714-5895-4bef-a09d-5e68ab09a541";
let APIT_KEY;
let API_SECRET;
//
let device;
let token;
let registered = false;
let contactName;
let contactPhone;
let pairName;
let pairPhonenumber;
let pairExtension;
let callId;

// FREEAGENT BEGIN
let FAClient;
let currentContactId;

const SERVICE = {
  name: "FreeAgentService",
  // Based on the hosting server
  appletId: `aHR0cHM6Ly9wbGF5aW5nLXZvaWNlLjAwMHdlYmhvc3RhcHAuY29tLw==`,
};

const PHONE_APPLET_CONFIGURATION = {
  name: "phone_call",
};

const PAIR_PHONE_APP_CONFIGURATION = {
  name: "pair_phone",
};

// Need to configure the field name by the Tenant
PHONE_APPLET_CONFIGURATION.fields = {
  from: `${PHONE_APPLET_CONFIGURATION.name}_field0`, // Phone
  to: `${PHONE_APPLET_CONFIGURATION.name}_field1`, // Phone
  contact: `${PHONE_APPLET_CONFIGURATION.name}_field2`, // Reference to Contacts
  duration: `${PHONE_APPLET_CONFIGURATION.name}_field3`, // Number with mask `00:00:00`
  direction: `${PHONE_APPLET_CONFIGURATION.name}_field4`, // Choice List,
  status: `${PHONE_APPLET_CONFIGURATION.name}_field5`, // Status
  note: `${PHONE_APPLET_CONFIGURATION.name}_field6`, // Note
  callId: `${PHONE_APPLET_CONFIGURATION.name}_field7`, // Text (unique)
  callAppType: `${PHONE_APPLET_CONFIGURATION.name}_field8`, // Choice List
};

// Need to Finalize later
PAIR_PHONE_APP_CONFIGURATION.fields = {
  userName: `${PAIR_PHONE_APP_CONFIGURATION.name}_field2`,
  pairNumber: `${PAIR_PHONE_APP_CONFIGURATION.name}_field0`,
  pairStatus: `${PAIR_PHONE_APP_CONFIGURATION.name}_field8`,
  callId: `${PAIR_PHONE_APP_CONFIGURATION.name}_field6`,
  outgoingCallerIdSid: `${PAIR_PHONE_APP_CONFIGURATION.name}_field7`,
};

FAClient = new FAAppletClient({
  appletId: SERVICE.appletId,
});

FAClient.on("phone_field_clicked", ({ record, number }) => {
  FAClient.open();
  initCall(record, number);
});

// Try to pass an event to dynamically change the UI based on the paired result
FAClient.on("hangup_pair_phone", ({ result }) => {
  console.log("hangup pair Phone now!!");
  inParingBlock.classList.add("hide");
  console.log(result);
  if (resultObject.result === "success") {
    displayPairedPhone();
  } else {
    backToDialog();
  }
});

ansBtn.onclick = (e) => {
  e.preventDefault();
  makeOutgoingCall();
};
endBtn.onclick = (e) => {
  e.preventDefault();
  hangup();
};
pairedbtn.onclick = (e) => {
  e.preventDefault();
  pairPhone();
  getPairPhoneFromApp();
};
backBtn.onclick = (e) => {
  e.preventDefault();
  backToDialog();
};
countryInput.addEventListener("change", (e) => {
  e.preventDefault();
  setPhoneFormat();
});

verifyPhoneBtn.onclick = () => {
  pairName = nameInput.value;
  pairPhonenumber = phoneInput.value;
  pairExtension = extensionInput.value;
  backBtn.classList.add("hide");
  pair(pairName, pairPhonenumber, pairExtension);
};

async function startupService() {
  startupClient();
}

async function startupClient() {
  callStatus.classList.remove("hide");

  try {
    const data = await $.getJSON("/token.php");
    console.log(data);

    token = data.token;
    intitializeDevice();
  } catch (err) {
    console.log(err);
    log(
      "Not able to connect to service. See your browser console for more information."
    );
  }
}

function intitializeDevice() {
  device = new Twilio.Device(token, {
    debug: true,
    answerOnBridge: true,
    // Set Opus as our preferred codec. Opus generally performs better, requiring less bandwidth and
    // providing better audio quality in restrained network conditions. Opus will be default in 2.0.
    codecPreferences: ["opus", "pcmu"],
  });

  getPairPhoneFromApp();
  addDeviceListeners(device);
  device.register();
}

const initCall = (data, num) => {
  contactName = _.get(data, "field_values.full_name.value");
  contactPhone = num ? num : _.get(data, "field_values.work_phone.value");
  currentContactId = _.get(data, "id");
  showContact();
  makeOutgoingCall();
};

async function getPairPhoneFromApp() {
  $("#paired-number-list").empty();
  await FAClient.listEntityValues(
    {
      entity: PAIR_PHONE_APP_CONFIGURATION.name,
    },
    (existingPairedPhone) => {
      existingPairedPhone.forEach((record) => {
        let recordCallerId = _.get(
          record,
          "field_values.pair_phone_field0.value"
        );
        let option = document.createElement("option");
        option.text = recordCallerId;
        pairNumberList.add(option);
      });
    }
  );
}

async function pair(name, phone, extension) {
  let url = `https://api.twilio.com/2010-04-01/Accounts/${ACCOUNT_SID}/OutgoingCallerIds.json`;
  let headers = new Headers();
  headers.set("Authorization", "Basic " + btoa(ACCOUNT_SID + ":" + AUTH_TOKEN));

  let urlencoded = new URLSearchParams();
  urlencoded.append("FriendlyName", name);
  urlencoded.append("PhoneNumber", phone);
  urlencoded.append("Extension", extension);
  urlencoded.append("StatusCallback", FAWEBHOOKURL);

  var requestOptions = {
    method: "POST",
    headers: headers,
    body: urlencoded,
    redirect: "follow",
  };

  let response;

  try {
    response = await fetch(url, requestOptions);
    let result = await response.json();
    if (!response.ok) {
      // Show some Error message in the UI
      const message = `An error has occured: ${result.message}`;
      pairHint.textContent = `Error: ${result.message} Please try again.`;
      backBtn.classList.remove("hide");
      throw new Error(message);
    }
    beforePairBlock.classList.add("hide");
    inParingBlock.classList.remove("hide");
    console.log(result);
    callingPhone.textContent = phone;
    verificationCode.textContent = result.validation_code;

    let newPairPhoneRecord = {
      entity: PAIR_PHONE_APP_CONFIGURATION.name,
      field_values: {
        [PAIR_PHONE_APP_CONFIGURATION.fields.userName]: result.friendly_name,
        [PAIR_PHONE_APP_CONFIGURATION.fields.pairNumber]: result.phone_number,
        [PAIR_PHONE_APP_CONFIGURATION.fields.callId]: result.call_sid,
        [PAIR_PHONE_APP_CONFIGURATION.fields.pairStatus]: "In Progress",
      },
    };

    let newPairPhoneId;
    await createAndCheck(newPairPhoneRecord, newPairPhoneId);
    backBtn.classList.remove("hide");
    // Check the field every second here and show the resule.

    // displayPairedPhone();
    //backToDialog();
  } catch (error) {
    console.log(error);
  }
}

async function createAndCheck(newPairPhoneRecord, newPairPhoneId) {
  FAClient.createEntity(newPairPhoneRecord, async (newPairPhone) => {
    console.log("1 ", newPairPhone);
    newPairPhoneId = _.get(
      newPairPhone,
      "entity_value.field_values.seq_id.value"
    );
    // do the fetch and check every second here
    console.log("2 ", newPairPhoneId);
    let isDone = false;
    // Need to fix
    //await checkPairedStatus(newPairPhoneId, isDone);
    console.log("6 ", isDone);
    // while (!done) {
    //   setTimeout(() => {
    //     (done = checkPairedStatus(newPairPhoneId)), 2000;
    //   });
    // }
    console.log("7 ,done!");
  });
}

async function checkPairedStatus(newPairPhoneId, doneFlag) {
  console.log("3 ", "call check here!");
  await FAClient.listEntityValues(
    {
      entity: PAIR_PHONE_APP_CONFIGURATION.name,
      filters: [
        {
          field_name: "seq_id",
          operator: "includes",
          values: [newPairPhoneId],
        },
      ],
    },
    (resultList) => {
      if (resultList.length === 0) {
        doneFlag = true;
      }
      console.log("4 ", resultList);
      const result = _.get(resultList, "[0]");
      const pairedStatus = _.get(
        result,
        // Need to finalize
        "field_values.pair_phone_field8.value"
      );
      console.log("5 ", pairedStatus);
      if (pairedStatus === "success") {
        doneFlag = true;
      }
    }
  );
}

async function pairPhone() {
  phoneBlock.classList.add("hide");
  pairPhoneBlock.classList.remove("hide");
  COUNTRYARRAY.forEach((country) => {
    let option = document.createElement("option");
    option.text = country;
    countryInput.add(option);
  });
}

function backToDialog() {
  phoneBlock.classList.remove("hide");
  pairPhoneBlock.classList.add("hide");
  beforePairBlock.classList.remove("hide");
  inParingBlock.classList.add("hide");
  backBtn.classList.add("hide");
  displayPair.classList.add("hide");
  pairHint.textContent =
    "What phone would you like to pair with FreeAgent for outbound calling?";
}

function addDeviceListeners(device) {
  device.on("registered", function () {
    registered = true;
  });

  device.on("error", function (error) {
    log("Error: " + error.message);
    callStatus.classList.remove("hide");
  });

  device.on("incoming", handleIncomingCall);
}

function showContact() {
  contactNameInfo.textContent = contactName ? contactName : "";
  contactPhoneInfo.textContent = contactPhone;
  inputBox.value = contactPhone;
  callStatus.classList.add("hide");
  contactInfo.classList.remove("hide");
  dialInputBox.classList.add("hide");
}

function clearContact() {
  contactNameInfo.textContent = "";
  contactPhoneInfo.textContent = "";
  inputBox.value = "";
  callStatus.classList.add("hide");
  contactInfo.classList.add("hide");
  dialInputBox.classList.remove("hide");
}

function setPhoneFormat() {
  const re = /\+\d+/;
  phoneInput.value = String(countryInput.value).match(re)[0];
}

function displayPairedPhone() {
  backBtn.classList.remove("hide");
  displayPair.classList.remove("hide");
}

// MAKE AN OUTGOING CALL

async function makeOutgoingCall() {
  var params = {
    // get the phone number to call from the DOM
    phone: inputBox.value,
  };

  if (device) {
    // Twilio.Device.connect() returns a Call object
    const call = await device.connect({ params });

    // "accepted" means the call has finished connecting and the state is now "open"
    //call.addListener("accept", updateUIAcceptedCall);
    call.addListener("disconnect", updateUIDisconnectedCall);
  } else {
    log("Unable to make call.");
  }
}

// Add a clock for dial

// Setup the last call record
async function handleEndCall() {
  // Hardcode now
  let url = `https://api.twilio.com/2010-04-01/Accounts/${ACCOUNT_SID}/Calls.csv?PageSize=1`;
  let Username = ACCOUNT_SID;
  let Password = AUTH_TOKEN;

  let headers = new Headers();

  headers.set("Authorization", "Basic " + btoa(Username + ":" + Password));

  const reponse = await fetch(url, {
    method: "GET",
    headers: headers,
  });

  const input = await reponse.text();
  var callLog = $.csv.toObjects(input)[0];
  // Need a indicator for detecting whether a call is on not not

  console.log(callLog);
  console.log("sid: " + callLog.ParentCallSid);
  console.log("parentI: " + callLog.Sid);
  if (callLog.To) {
    const newRecordField = {
      entity: PHONE_APPLET_CONFIGURATION.name,
      field_values: {
        [PHONE_APPLET_CONFIGURATION.fields.from]: callLog.FromFormatted,
        [PHONE_APPLET_CONFIGURATION.fields.to]: callLog.ToFormatted,
        [PHONE_APPLET_CONFIGURATION.fields.contact]: currentContactId,
        [PHONE_APPLET_CONFIGURATION.fields.duration]: parseInt(
          callLog.Duration
        ),
        [PHONE_APPLET_CONFIGURATION.fields.direction]: callLog.Direction,
        [PHONE_APPLET_CONFIGURATION.fields.status]: callLog.Status,
        [PHONE_APPLET_CONFIGURATION.fields.callId]: callLog.ParentCallSid,
        [PHONE_APPLET_CONFIGURATION.fields.note]: "",
        [PHONE_APPLET_CONFIGURATION.fields.callAppType]: "Voice",
      },
    };

    let newRecordId;

    await FAClient.createEntity(newRecordField, (newRecord) => {
      newRecordId = _.get(newRecord, "entity_value.field_values.seq_id.value");
      logCallById(
        newRecordId,
        newRecordField,
        ({ entity_value: phoneCall }) => {
          const entityInstance = {
            ...phoneCall,
            field_values: {
              ...phoneCall.field_values,
            },
          };
          FAClient.showModal("entityFormModal", {
            entity: PHONE_APPLET_CONFIGURATION.name,
            entityLabel: "Phone Call Outcome Log",
            entityInstance,
            showButtons: false,
          });
        }
      );
    });
  }
}

function logCallById(recordId, callValues, callback) {
  FAClient.listEntityValues(
    {
      entity: PHONE_APPLET_CONFIGURATION.name,
      filters: [
        {
          field_name: "seq_id",
          operator: "includes",
          values: [recordId],
        },
      ],
    },
    (existingPhoneCalls) => {
      const existingPhoneCall = _.get(existingPhoneCalls, "[0]");
      FAClient.upsertEntity(
        {
          id: _.get(existingPhoneCall, "id", ""),
          ...callValues,
        },
        callback
      );
    }
  );
}

function updateUIDisconnectedCall() {
  device.disconnectAll();
  handleEndCall();
  clearContact();
}

// HANG UP A CALL
function hangup() {
  if (device) {
    device.disconnectAll();
  }
}

// HANDLE INCOMING CALL
function handleIncomingCall(call) {
  log(`Incoming call from ${call.parameters.From}`);

  //show incoming call div and incoming phone number
  incomingCallDiv.classList.remove("hide");
  incomingPhoneNumberEl.innerHTML = call.parameters.From;

  //add event listeners for Accept, Reject, and Hangup buttons
  incomingCallAcceptButton.onclick = () => {
    acceptIncomingCall(call);
  };

  incomingCallRejectButton.onclick = () => {
    rejectIncomingCall(call);
  };

  incomingCallHangupButton.onclick = () => {
    hangupIncomingCall(call);
  };

  // add event listener to call object
  call.addListener("cancel", handleDisconnectedIncomingCall);
}

// ACCEPT INCOMING CALL

function acceptIncomingCall(call) {
  call.accept();
}

// REJECT INCOMING CALL

function rejectIncomingCall(call) {
  call.reject();
  resetIncomingCallUI();
}

// HANG UP INCOMING CALL

function hangupIncomingCall() {
  hangup();
  resetIncomingCallUI();
}

// HANDLE CANCELLED INCOMING CALL

function handleDisconnectedIncomingCall(call) {
  device.disconnectAll();
  console.log("handledisconnected : ", call);
  log("Incoming call ended.");
  resetIncomingCallUI();
}

// MISC USER INTERFACE

// Activity log
function log(message) {
  dialInputBox.classList.add("hide");
  callStatus.textContent = message;
  callStatus.classList.remove("hide");
}

function resetIncomingCallUI() {
  incomingPhoneNumberEl.innerHTML = "";
  incomingCallDiv.classList.add("hide");
}

// AUDIO CONTROLS

async function getAudioDevices() {
  await navigator.mediaDevices.getUserMedia({ audio: true });
}
